package ee.ut.cs.web.veebirakendused.Tables;

import ee.ut.cs.web.veebirakendused.Tables.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class Usercontroller {
    private final static String SIGNUP_PAGE = "user/signup";
    private final static String LOGIN_PAGE = "user/login";
    private final UserService userService;
    @Autowired
    Usercontroller(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup() {
        return SIGNUP_PAGE;
    }

    @GetMapping("/users")
    public String getUsers(Model model) {
        model.addAttribute("user", userService.getAllUsers());
        return LOGIN_PAGE;
    }

    @PostMapping("/signup")
    public String addNewUser(@ModelAttribute UserDTO userDTO, Model model) {
        // map DTO to entity that can be persisted using service
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());

        try {

            userService.addUser(user);
            model.addAttribute("successMessage", "User added succesfully");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        } finally {
            model.addAttribute("user", userService.getAllUsers());
        }

        return SIGNUP_PAGE;
    }

}
