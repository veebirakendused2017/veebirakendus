package ee.ut.cs.web.veebirakendused.Tables;

import ee.ut.cs.web.veebirakendused.Tables.dto.MovieDTO;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class Moviecontroller {
    private final static String MOVIE_PAGE = "/movies";
    private final static String ADMIN_PAGE = "user/admin";
    private static String UPLOAD = "/Image";
    private final MovieService movieService;
    @Autowired
    Moviecontroller(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies")
    public String getMovies(Model model) {
        model.addAttribute("movies", movieService.getAllMovies());
        return MOVIE_PAGE;
    }
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String admin() {
        return ADMIN_PAGE;
    }

    @PostMapping("/admin")
    public String addNewMovie(@ModelAttribute MovieDTO movieDTO, Model model) {
        // map DTO to entity that can be persisted using service
        Movie movie = new Movie();
        movie.setMoviename(movieDTO.getMoviename());
        movie.setDescription(movieDTO.getDescription());
        movie.setZanr(movieDTO.getZanr());
        movie.setHinne(movieDTO.getHinne());
        movie.setUrl(movieDTO.getUrl());


        try {
            // get image from DTO
            movie.setImageSource(getImageBase64Format(movieDTO.getImage()));
            // persist entity object to DB
            movieService.addMovie(movie);
            // if no exeption was thrown, everything went successfully
            model.addAttribute("successMessage", "Movie added successfully!");
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
        } finally {
            model.addAttribute("movie", movieService.getAllMovies());
        }

        return ADMIN_PAGE;
    }

    // here we take uploaded image bytes and map it to the string
    // that can later be used to show the image in HTML <img src="">
    private String getImageBase64Format(MultipartFile file) throws Exception {
       return "data:" + file.getContentType() + ";base64," + Base64.encodeBase64String(file.getBytes());}

}

