package ee.ut.cs.web.veebirakendused.home;

import org.springframework.stereotype.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class InfoController {
    private static final String TEMPLATE_INFO = "home/info";

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public String main(){

        return TEMPLATE_INFO;
    }
}
