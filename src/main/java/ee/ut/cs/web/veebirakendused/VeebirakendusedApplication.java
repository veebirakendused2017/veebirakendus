package ee.ut.cs.web.veebirakendused;

import ee.ut.cs.web.veebirakendused.Tables.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeebirakendusedApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeebirakendusedApplication.class, args);
	}
}
