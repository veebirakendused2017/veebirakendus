package ee.ut.cs.web.veebirakendused.Tables;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    private final Path rootLocation = Paths.get("Image");

    @Autowired
    MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void addMovie(Movie movie) {
        Movie existing = movieRepository.findMoviesByMoviename(movie.getMoviename());
        if (existing != null) {
            // if subscription with same email already exists, throw exception
            throw new UnsupportedOperationException("Cannot put movie twice");
        }
        movieRepository.save(movie);
    }

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

}

