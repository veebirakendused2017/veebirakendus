package ee.ut.cs.web.veebirakendused.Tables;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        User existing = userRepository.findUserByEmail(user.getEmail());
        if (existing != null) {
            // if subscription with same email already exists, throw exception
            throw new UnsupportedOperationException("Cannot put user twice");
        }
        userRepository.save(user);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
