package ee.ut.cs.web.veebirakendused.home;

import ee.ut.cs.web.veebirakendused.Tables.Movie;
import ee.ut.cs.web.veebirakendused.Tables.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomeController {

    private static final String TEMPLATE_MAIN = "home/main";
    @Autowired
    private MovieService movieService;
    @RequestMapping(path = "", method = RequestMethod.GET)
    public String main(
            @RequestParam(value="name", required=false, defaultValue="Filmielamus") String name, Model model) {
        model.addAttribute("name", name);
        List<Movie> films = movieService.getAllMovies();
        model.addAttribute("films", films);
        
        return TEMPLATE_MAIN;
    }

}
