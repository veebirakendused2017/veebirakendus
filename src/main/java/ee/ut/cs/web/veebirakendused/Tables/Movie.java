package ee.ut.cs.web.veebirakendused.Tables;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Table
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue
    private Long id;
    private String moviename;
    private String description;
    @ElementCollection(targetClass=String.class)
    private List<String> actors;
    private String zanr;
    @ElementCollection(targetClass=String.class)
    private List<String> comments;
    private int imdbHinne;
    private int hinne;
    private String imageSource;
    private String url;

}