package ee.ut.cs.web.veebirakendused.Tables;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>  {

        //@Query(value = "SELECT * FROM subscription WHERE email = :email", nativeQuery = true)
        Movie findMoviesByMoviename(String moviename);


}
