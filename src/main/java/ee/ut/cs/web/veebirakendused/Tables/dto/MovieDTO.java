package ee.ut.cs.web.veebirakendused.Tables.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter

public class MovieDTO {
    private String moviename;
    private String description;
    private List<String> actors;
    private String zanr;
    private List<String> comments;
    private int imdbHinne;
    private int hinne;
    private MultipartFile Image;
    private String url;
}
