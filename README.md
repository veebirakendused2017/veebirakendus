$ git tag

# veebirakendus


Tiimi liikmed: 
•	Oliver Ossip
•	Ted Edward Õunap

Kasutatavad tehnoloogiad: 

Front-end:
•	HTML
•	CSS (Bootstrap)
•	Javascript

Back-end: 
•	Java
•	MySQL

Ülesanded katsume jaotada võrdselt. Oliver tegeleb rohkem back-endiga ja Ted front-endiga. Arendamiseks saame iga nädal vähemalt korra kokku, ülejäänud töö iseseisvalt kodus. 

Etapp 1:

•	[Projektiplaan](https://bitbucket.org/veebirakendused2017/veebirakendus/wiki/Plaan) 5p

•	[Prototüüp](https://bitbucket.org/veebirakendused2017/veebirakendus/wiki/Plaan) 3p

•	Koodirepo 1p

Punkte peale 1 etappi: 9

Etapp 2:

•	[Testkeskkond](https://safe-forest-27751.herokuapp.com/main) 5p

Etapp 3: 

•	[Testkeskkond](https://safe-forest-27751.herokuapp.com/) 

•	HTML/CSS kujundus

•	Navigeerimismenüü

•	Aknasuuruse tugi

•	Info lehele lisatud kaart

•	Filmide algse info väljakutsumine lehele

•	Andmebaasi andmete lisamine

Etapp 4:

• Fb login

• Admin lehekülg lisamiseks(urli kaudu hetkel)

• Otsingumootoritele optimeerimine